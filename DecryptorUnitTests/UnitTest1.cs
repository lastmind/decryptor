using Decryptor.Controllers;
using Microsoft.AspNetCore.Http;
using NUnit.Framework;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DecryptorUnitTests
{
    public class EncryptionTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            string DecryptedText = "������ ���, ��� �� ���������!";
            string EncryptedText = "������ ���, ��� �� ���������!";
            string Password = "���������������";
            string Dictionary = "RussianFull";

            Assert.AreEqual(DecryptedText, TextDecryptor.Decrypt(EncryptedText, Password, Dictionary));
            Assert.AreEqual(EncryptedText, TextDecryptor.Encrypt(DecryptedText, Password, Dictionary));
        }

        [Test]
        public void Test2()
        {
            string DecryptedText = "������ ���, ��� �� ���������!";
            string EncryptedText = "������ ���, ��� �� ���������!";
            string Password = "���������������";
            string Dictionary = "RussianShort";

            Assert.AreEqual(DecryptedText, TextDecryptor.Decrypt(EncryptedText, Password, Dictionary));
            Assert.AreEqual(EncryptedText, TextDecryptor.Encrypt(DecryptedText, Password, Dictionary));
        }

        [Test]
        public void Test3()
        {
            string DecryptedText = "my home is my heart!";
            string EncryptedText = "if lfqm aa fu oirvb!";
            string Password = "whereisit";
            string Dictionary = "English";

            Assert.AreEqual(DecryptedText, TextDecryptor.Decrypt(EncryptedText, Password, Dictionary));
            Assert.AreEqual(EncryptedText, TextDecryptor.Encrypt(DecryptedText, Password, Dictionary));
        }

        [Test]
        public void Test4()
        {
            string DecryptedText = "";
            string EncryptedText = "";
            string Password = "whereisit";
            string Dictionary = "English";

            Assert.AreEqual(DecryptedText, TextDecryptor.Decrypt(EncryptedText, Password, Dictionary));
            Assert.AreEqual(EncryptedText, TextDecryptor.Encrypt(DecryptedText, Password, Dictionary));
        }

        [Test]
        public void Test5()
        {
            string DecryptedText = "my home is my heart!";
            string EncryptedText = "if lfqm aa fu oirvb!";
            string Password = "whe re i������sit";
            string Dictionary = "English";

            Assert.Throws<Exception>(() => TextDecryptor.Decrypt(EncryptedText, Password, Dictionary), "�������� ����");
            Assert.Throws<Exception>(() => TextDecryptor.Encrypt(DecryptedText, Password, Dictionary), "�������� ����");
        }

        [Test]
        public void Test6()
        {
            string DecryptedText = "my home is my heart!";
            string EncryptedText = "if lfqm aa fu oirvb!";
            string Password = "";
            string Dictionary = "English";

            Assert.Throws<Exception>(() => TextDecryptor.Decrypt(EncryptedText, Password, Dictionary), "������ ����");
            Assert.Throws<Exception>(() => TextDecryptor.Encrypt(DecryptedText, Password, Dictionary), "������ ����");
        }

        [Test]
        public void Test7()
        {
            string DecryptedText = "my home is my heart!";
            string EncryptedText = "if lfqm aa fu oirvb!";
            string Password = "ghdfgdfg";
            string Dictionary = "Engli�����sh";

            Assert.Throws<Exception>(() => TextDecryptor.Decrypt(EncryptedText, Password, Dictionary), "������ ������� �� ������");
            Assert.Throws<Exception>(() => TextDecryptor.Encrypt(DecryptedText, Password, Dictionary), "������ ������� �� ������");
        }

        [Test]
        public void Test8()
        {
            string DecryptedText = "������ ���, ��� �� ���������!";
            string EncryptedText = "������ ���, ��� �� ���������!";
            string Password = "���������������";
            string Dictionary = "RussianFull";

            Assert.AreEqual(DecryptedText, TextDecryptor.Decrypt(EncryptedText, Password, Dictionary));
            Assert.AreEqual(EncryptedText, TextDecryptor.Encrypt(DecryptedText, Password, Dictionary));
        }
    }

    public class FileTests
    {
        [SetUp]
        public void Setup()
        {
        }



        [Test]
        public void Test1()
        {
            string Result = "����������, �� ������� �������� �����!!! \r\n� �������� ������, ��� ��� ������������ ���� �������� �� ����� ������, �������� ��������� ����������� ������ � ������� ����� � ����� �����! � ������ ������ ������ ������� ���������� ��, ��� �� ���������� ������ ��� ������� �����������. ������ ���� �������� �� �����, �������� ��������� �� ����������� �����, �������� ��� ������� ������� � ������������ ���� ������! �������, ��� ���� ���������� ������� � ���������� ��� � ��������� �����, �� ������� ��� ���� ��� ��������� ��������, � � ������� ����� ���������! �� ���� �������� FirstLineSoftware � ������������ ����, � ��� ���������� ���� � ����������� ���������� ����� ������ �# ��� ����������! �� ����� �������� ������� � ���������� ���������� � ��� �� � ���������������� � �������������� ����� ���������� .Net, ���� �������� � ���������� �����!\r\n";
            using (var strtxt = File.OpenRead(Directory.GetCurrentDirectory() + "\\TestFile.txt"))
                Assert.AreEqual(Result, Decryptor.Controllers.TextDecryptor.DataFromFile(new TestFile("TestFile.txt", strtxt)));
            using (var strdocx = File.OpenRead(Directory.GetCurrentDirectory() + "\\TestFile.docx"))
                Assert.AreEqual(Result, Decryptor.Controllers.TextDecryptor.DataFromFile(new TestFile("TextFile.docx", strdocx)));
        }
        [Test]
        public void Test2()
        {
            string Result = "����������, �� ������� �������� �����!!! \r\n� �������� ������, ��� ��� ������������ ���� �������� �� ����� ������, �������� ��������� ����������� ������ � ������� ����� � ����� �����! � ������ ������ ������ ������� ���������� ��, ��� �� ���������� ������ ��� ������� �����������. ������ ���� �������� �� �����, �������� ��������� �� ����������� �����, �������� ��� ������� ������� � ������������ ���� ������! �������, ��� ���� ���������� ������� � ���������� ��� � ��������� �����, �� ������� ��� ���� ��� ��������� ��������, � � ������� ����� ���������! �� ���� �������� FirstLineSoftware � ������������ ����, � ��� ���������� ���� � ����������� ���������� ����� ������ �# ��� ����������! �� ����� �������� ������� � ���������� ���������� � ��� �� � ���������������� � �������������� ����� ���������� .Net, ���� �������� � ���������� �����!\r\n";
            var tmp = new Decryptor.Controllers.HomeController(null);
            Stream test = (tmp.DownloadFile("txt", Result) as Microsoft.AspNetCore.Mvc.FileStreamResult).FileStream;
            Assert.AreEqual(Result, new StreamReader(test).ReadToEnd());
        }
        private class TestFile : IFormFile
        {
            public string ContentDisposition { get; set; }

            public string ContentType { get; set; }

            public string FileName { get; set; }

            public IHeaderDictionary Headers { get; set; }

            public long Length { get; set; }

            public string Name { get; set; }

            public Stream stream;

            public void CopyTo(Stream target)
            {
                throw new NotImplementedException();
            }

            public Task CopyToAsync(Stream target, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }

            public Stream OpenReadStream()
            {
                return stream;
            }

            public TestFile(string fileName, Stream stream)
            {
                FileName = fileName;
                this.stream = stream;
            }
            ~TestFile()
            {
                stream.Dispose();
            }
        }
    }
}