﻿using Decryptor.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Xceed.Words.NET;

namespace Decryptor.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult Index(string text, IFormFile file, string dict, string key, bool encrypt)
        {
            text ??= "";
            try
            {
                if (file != null) text = TextDecryptor.DataFromFile(file);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            ViewBag.BaseValue = text;
            try
            {
                if (encrypt)
                    text = TextDecryptor.Encrypt(text, key, dict);
                else
                    text = TextDecryptor.Decrypt(text, key, dict);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            ViewBag.Key = key;
            ViewBag.Result = text;
            if (!encrypt) ViewBag.Decrypt = true;
            ViewBag.Dict = dict;
            return View();
        }
        public IActionResult DownloadFile(string type, string text)
        {
            var result = new MemoryStream();
            if (type == "txt")
            {
                using (var t = new StreamWriter(result))
                {
                    t.Write(text.ToCharArray());
                    t.Flush();
                    result.Position = 0;
                    return File(new MemoryStream(result.ToArray()), "text/plain", "result.txt");
                }
            }
            else if (type == "docx")
            {
                using (var t = DocX.Create(result))
                {
                    t.InsertParagraph(text);
                    t.Save();
                }
                result.Position = 0;
                return File(result, "application/vnd.ms-word", "result.docx");
            }
            return null;
        }
    }
}
