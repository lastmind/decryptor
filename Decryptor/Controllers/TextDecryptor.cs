﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xceed.Words.NET;

namespace Decryptor.Controllers
{
    public class TextDecryptor
    {
        public static string Encrypt(string data, string password, string Dict)
        {
            if (string.IsNullOrWhiteSpace(password)) throw new Exception("Пустой ключ");
            if (!Alphabets.ContainsKey(Dict)) throw new Exception("Данный алфавит не найден");
            string a = Alphabets[Dict];
            password = password.ToLower();
            foreach (char c in password)
            {
                if (!a.Contains(c)) throw new Exception("Неверный ключ");
            }
            CharEnumerator it = password.GetEnumerator();
            StringBuilder result = new StringBuilder();
            string _data = data.ToLower();
            for (int i = 0; i < data.Length; i++)
            {
                char c = _data[i];
                if (a.Contains(c))
                {
                    if (!it.MoveNext()) { it.Reset(); it.MoveNext(); }
                    if (char.IsUpper(data[i]))
                        result.Append(char.ToUpper(a[(a.IndexOf(c) + a.IndexOf(it.Current)) % a.Length]));
                    else
                        result.Append(a[(a.IndexOf(c) + a.IndexOf(it.Current)) % a.Length]);
                }
                else
                    result.Append(c);
            }
            return result.ToString();
        }
        public static string Decrypt(string data, string password, string Dict)
        {
            if (string.IsNullOrWhiteSpace(password)) throw new Exception("Пустой ключ");
            if (!Alphabets.ContainsKey(Dict)) throw new Exception("Данный алфавит не найден");
            string a = Alphabets[Dict];
            password = password.ToLower();
            foreach (char c in password)
            {
                if (!a.Contains(c)) throw new Exception("Неверный ключ");
            }
            CharEnumerator it = password.GetEnumerator();
            StringBuilder result = new StringBuilder();
            string _data = data.ToLower();
            for (int i = 0; i < data.Length; i++)
            {
                char c = _data[i];
                if (a.Contains(c))
                {
                    if (!it.MoveNext()) { it.Reset(); it.MoveNext(); }
                    if (char.IsUpper(data[i]))
                        result.Append(char.ToUpper(a[(a.IndexOf(c) - a.IndexOf(it.Current) + a.Length) % a.Length]));
                    else
                        result.Append(a[(a.IndexOf(c) - a.IndexOf(it.Current) + a.Length) % a.Length]);
                }
                else
                    result.Append(c);
            }
            return result.ToString();
        }
        public static string DataFromFile(IFormFile file)
        {
            StringBuilder result = new StringBuilder();
            if (Path.GetExtension(file.FileName).ToLower() == ".txt")
            {
                using (var tmp = new StreamReader(file.OpenReadStream()))
                {
                    result.Append(tmp.ReadToEnd());
                }
            }
            else if (Path.GetExtension(file.FileName).ToLower() == ".docx")
            {
                using (DocX tmp = DocX.Load(file.OpenReadStream()))
                {
                    foreach (var paragraph in tmp.Paragraphs)
                    {
                        result.Append(paragraph.Text);
                        result.Append("\r\n");
                    }
                }
            }
            else throw new Exception("Неизвестный формат файла");
            return result.ToString();
        }

        public static Dictionary<string, string> Alphabets = new Dictionary<string, string>();

        static TextDecryptor()
        {
            Alphabets.Add("RussianFull", "абвгдеёжзийклмнопрстуфхцчшщъыьэюя");
            Alphabets.Add("RussianShort", "абвгдежзийклмнопрстуфхцчшщъыьэюя");
            Alphabets.Add("English", "abcdefghijklmnopqrstuvwxyz");
        }
    }
}
